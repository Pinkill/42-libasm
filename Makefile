SRC = ft_read.s ft_strcmp.s ft_strcpy.s ft_strdup.s ft_strlen.s ft_write.s

BONUS = ft_atoi_base_bonus.s ft_isspace.s ft_list_push_front_bonus.s ft_list_remove_if_bonus.s \
ft_list_size_bonus.s ft_list_sort_bonus.s

OS = $(shell uname)

SRCS = ${addprefix ${PRE}, ${SRC}}

OBJS = ${SRCS:.s=.o}

BONUS_OBJS = ${BONUS:.s=.o}

PRE = ./

HEAD = ./

NAME = libasm.a

AR = ar rc

RAN = ranlib

GCC = clang

NASM = nasm

CFLAG = -Wall -Wextra -Werror

ifeq ($(OS), Darwin)
NASMFLAG = -f macho64 -d IS_APPLE
else
NASMFLAG = -f elf64
endif

all:	$(NAME)

bonus:	${OBJS} ${BONUS_OBJS}
	${AR} ${NAME} ${OBJS} ${BONUS_OBJS}
	${RAN} ${NAME}

%.o: %.s
	${NASM} ${NASMFLAG} $< -o ${<:.s=.o}

$(NAME): ${OBJS}
	${AR} ${NAME} ${OBJS}
	${RAN} ${NAME}

run:
	${GCC} ${CFLAG} main.c -L. -lasm
	./a.out

clean:
	rm -f ${NAME}
	rm -f a.out

fclean:	clean
	rm -f ${OBJS}
	rm -f ${BONUS_OBJS}

re: 	fclean all

.PHONY:		all bonus clean fclean re
