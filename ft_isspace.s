; **************************************************************************** #
;                                                                              #
;                                                         :::      ::::::::    #
;    ft_isspace.s                                       :+:      :+:    :+:    #
;                                                     +:+ +:+         +:+      #
;    By: mknezevi <mknezevi@student.42.fr>          +#+  +:+       +#+         #
;                                                 +#+#+#+#+#+   +#+            #
;    Created: 2020/10/19 19:47:57 by mknezevi          #+#    #+#              #
;    Updated: 2020/10/19 19:47:58 by mknezevi         ###   ########.fr        #
;                                                                              #
; **************************************************************************** #

global _ft_isspace

;int	ft_isspace(int c);

section .text

_ft_isspace:
	push rbp			;Function prologue, saving the stack status before function call
	mov rbp, rsp		;~
	mov rax, 1
	cmp rdi, 9			;9 in ascii is horizontal tab, ie. '\t'
	je .end
	cmp rdi, 10			;10 in ascii is new line, ie. '\n'
	je .end
	cmp rdi, 11			;11 in ascii is vertical tab, ie. '\v'
	je .end
	cmp rdi, 12			;12 in ascii is form feed, ie. '\f'
	je .end
	cmp rdi, 13			;13 in asci is carriage return, ie. '\r'
	je .end
	cmp rdi, 32			;32 in ascii is space, ie. ' '
	je .end
	jmp .set_zero

	.set_zero:
		mov rax, 0
		jmp .end

	.end:					;Function epilogue, pulling the stack status before function call
		mov rsp, rbp
		pop rbp
		ret
