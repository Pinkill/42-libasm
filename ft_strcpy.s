; **************************************************************************** #
;                                                                              #
;                                                         :::      ::::::::    #
;    ft_strcpy.s                                        :+:      :+:    :+:    #
;                                                     +:+ +:+         +:+      #
;    By: mknezevi <mknezevi@student.42.fr>          +#+  +:+       +#+         #
;                                                 +#+#+#+#+#+   +#+            #
;    Created: 2020/09/30 13:08:23 by mknezevi          #+#    #+#              #
;    Updated: 2020/10/02 20:46:32 by mknezevi         ###   ########.fr        #
;                                                                              #
; **************************************************************************** #


global _ft_strcpy

section .text

_ft_strcpy:
	push rbp			;Function prologue, saving the stack status before function call
	mov rbp, rsp		;~
	lea rax, [rdi]		;Get the address ro rdi value (yeah, weird)
	jmp .loop

	.loop:
		inc rdi					;Increment rdi and rsi
		inc rsi
		mov rdx, [rsi-1]		;Move 32-bit? of value from rsi to rdx
		mov [rdi-1], rdx		;Move the same value to rdi, this was needed because I can't access both address values in one mov command
		cmp byte [rdi-1], 0x0	;Checking if we reached the end of string
		jne .loop				;If not we repeat the loop
		jmp .end

	.end:					;Function epilogue, pulling the stack status before function call
		mov rsp, rbp
		pop rbp
		ret
