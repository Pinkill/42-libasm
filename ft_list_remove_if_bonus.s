; **************************************************************************** #
;                                                                              #
;                                                         :::      ::::::::    #
;    ft_list_remove_if_bonus.s                          :+:      :+:    :+:    #
;                                                     +:+ +:+         +:+      #
;    By: mknezevi <mknezevi@student.42.fr>          +#+  +:+       +#+         #
;                                                 +#+#+#+#+#+   +#+            #
;    Created: 2020/10/21 02:37:25 by mknezevi          #+#    #+#              #
;    Updated: 2020/11/01 04:36:40 by mknezevi         ###   ########.fr        #
;                                                                              #
; **************************************************************************** #

%include "os_specific_code.mac"

global _ft_list_remove_if

;void	ft_list_remove_if(t_list **begin_list, void *data_ref, int (*cmp)(), void (*free_fct)(void*));
;*data_ref		- reference of data, lists containing it have to be deleted
;(*cmp)			- function reference that compares list data (like ft_strcmp etc.)
;(*free_fct)	- function reference that frees list data (like free etc.)
;Recursive deletion of lists

section .text
	extern FREE

_ft_list_remove_if:
	push rbp						;Function prologue, saving the stack status before function call
	mov rbp, rsp					;~
	test rdi, rdi					;Check if pointer to pointer is null
	jz .end
	test rsi, rsi					;Check if data_ref is null
	jz .end
	test rdx, rdx					;Check if cmp function pointer is null
	jz .end
	test rcx, rcx					;Check if free_fct function pointer is null
	jz .end
	mov qword [head_ref], rdi		;Storing call arguments
	mov qword [data_ref], rsi
	mov qword [func_ptr], rdx
	mov qword [free_fct_ptr], rcx
	mov r10, qword [rdi]			;Get pointer out of pointer to pointer
	call loop						;Calling loop to check the list for data_ref value
	mov rdi, [head_ref]
	mov qword [rdi], r10			;Changing list in pointer to pointer
	jmp .end

	.end:					;Function epilogue, pulling the stack status before function call
		mov rsp, rbp
		pop rbp
		ret

loop:
	test r10, r10				;Check if list reached end
	jz .return_null
	mov rdi, qword [r10]		;Putting current list data pointer as first argument
	mov rsi, qword [data_ref]
	call [func_ptr]				;Calling the compare function
	test rax, rax				;Check if return value is 0 (ie. is equal)
	jz .remove_it				;If so remove the current list
	push r10
	mov r10, qword [r10+8]
	call loop
	mov r11, r10
	pop r10
	mov qword [r10+8], r11
	ret

	.remove_it:
		mov rdi, qword [r10]	;Putting current list data pointer as first argument
		push r10				;Calling free seems to replace a few registers
		call [free_fct_ptr]		;Calling the free_fct pointer
		pop r10
		mov rdi, r10			;Putting list pointer as first argument
		mov r10, qword [r10+8]
		push r10				;Calling free seems to replace a few registers
		call FREE				;Freeing the pointer
		pop r10
		call loop
		ret

	.return_null:
		mov r10, 0x0
		ret

section .data
	head_ref		dq	0
	func_ptr		dq	0
	free_fct_ptr	dq	0
	data_ref		dq	0
