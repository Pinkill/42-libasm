; **************************************************************************** #
;                                                                              #
;                                                         :::      ::::::::    #
;    ft_list_size_bonus.s                               :+:      :+:    :+:    #
;                                                     +:+ +:+         +:+      #
;    By: mknezevi <mknezevi@student.42.fr>          +#+  +:+       +#+         #
;                                                 +#+#+#+#+#+   +#+            #
;    Created: 2020/10/19 19:48:06 by mknezevi          #+#    #+#              #
;    Updated: 2020/10/27 16:12:23 by mknezevi         ###   ########.fr        #
;                                                                              #
; **************************************************************************** #

global _ft_list_size

;int	ft_list_size(t_list *begin_list);

section .text

_ft_list_size:
	push rbp				;Function prologue, saving the stack status before function call
	mov rbp, rsp			;~
	mov rax, 0x0			;Set rax 0
	test rdi, rdi			;Test if pointer is empty
	jz .end
	mov rdx, rdi			;Copy the address to another temporary variable to scroll through list
	jmp .loop

	.loop:
		test rdx, rdx			;Check if the pointer is set
		jz .end
		inc rax					;Increments the return value
		mov rdx, qword [rdx+8]	;Move down the chain to next element
		jmp .loop

	.end:					;Function epilogue, pulling the stack status before function call
		mov rsp, rbp
		pop rbp
		ret
