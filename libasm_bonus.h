/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libasm_bonus.h                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mknezevi <mknezevi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/23 13:35:23 by user42            #+#    #+#             */
/*   Updated: 2020/11/01 04:29:51 by mknezevi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBASM_BONUS_H
# define LIBASM_BONUS_H
# include "libasm.h"

typedef struct	s_list
{
	void			*data;
	struct s_list	*next;
}				t_list;

extern int			ft_atoi_base(char *str, char *base) asm("_ft_atoi_base");
extern int			ft_isspace(int c) asm("_ft_isspace");
extern void			ft_list_push_front(t_list **begin_list, void *data) asm("_ft_list_push_front");
extern int			ft_list_size(t_list *begin_list) asm("_ft_list_size");
extern void			ft_list_sort(t_list **begin_list, int(*cmp)()) asm("_ft_list_sort");
extern void			ft_list_remove_if(t_list **begin_list, void *data_ref,
					int (*cmp)(), void (*free_fct)(void*)) asm("_ft_list_remove_if");
#endif
