/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libasm.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mknezevi <mknezevi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/09/19 17:54:54 by user42            #+#    #+#             */
/*   Updated: 2020/11/01 04:00:55 by mknezevi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBASM_H
# define LIBASM_H
# include <unistd.h>
# include <errno.h>
# include <stdlib.h>

extern size_t	ft_strlen(const char *s) asm("_ft_strlen");
extern char	*ft_strcpy(char *dest, const char *src) asm("_ft_strcpy");
extern int		ft_strcmp(const char *s1, const char *s2) asm("_ft_strcmp");
extern ssize_t	ft_write(int fd, const void *buf, size_t count) asm("_ft_write");
extern ssize_t	ft_read(int fd, void *buf, size_t count) asm("_ft_read");
extern char	*ft_strdup(const char *s) asm("_ft_strdup");
#endif
