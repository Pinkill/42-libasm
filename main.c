/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mknezevi <mknezevi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/09/19 17:54:11 by user42            #+#    #+#             */
/*   Updated: 2020/11/01 04:30:12 by mknezevi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libasm_bonus.h"
#include <fcntl.h>
#include <stdio.h>
#include <string.h>

void	ft_strlen_test()
{
	char	*str1 = "asbfgn";
	char	*str2 = "";
	int		ret;

	printf("This is a ft_strlen test with VALID variable:\n");
	ret = strlen(str1);
	printf("Expected:\tstr1: %s\tlength: %d\n", str1, ret);
	ret = ft_strlen(str1);
	printf("Real:\t\tstr1: %s\tlength: %d\n", str1, ret);
	printf("This is a ft_strlen test with EMPTY variable:\n");
	ret = strlen(str2);
	printf("Expected:\tstr1: %s\tlength: %d\n", str2, ret);
	ret = ft_strlen(str2);
	printf("Real:\t\tstr2: %s\tlength: %d\n\n\n", str2, ret);
}

void	ft_strcpy_test()
{
	char	*str1 = "Tu trouves cette";
	char	str2[17];
	char	*str3;
	char	*str4 = "";

	printf("This is a ft_strcpy test with VALID variable:\n");
	str3 = strcpy(str2, str1);
	printf("Expected:\tsrc: %s\tdest: %s\tret: %s\n", str1, str2, str3);
	printf("Expected:\tsrc_ptr: %p\tdest_ptr: %p\tret_ptr: %p\n", str1, str2, str3);
	str3 = ft_strcpy(str2, str1);
	printf("Real:\t\tsrc: %s\tdest: %s\tret: %s\n", str1, str2, str3);
	printf("Real:\t\tsrc_ptr: %p\tdest_ptr: %p\tret_ptr: %p\n", str1, str2, str3);
	printf("This is a ft_strcpy test with EMPTY variable:\n");
	str3 = strcpy(str2, str4);
	printf("Expected:\tsrc: %s\tdest: %s\tret: %s\n", str4, str2, str3);
	printf("Expected:\tsrc_ptr: %p\tdest_ptr: %p\tret_ptr: %p\n", str4, str2, str3);
	str3 = ft_strcpy(str2, str4);
	printf("Real:\t\tsrc: %s\tdest: %s\tret: %s\n", str4, str2, str3);
	printf("Real:\t\tsrc_ptr: %p\tdest_ptr: %p\tret_ptr: %p\n\n\n", str4, str2, str3);
}

void	ft_strcmp_test()
{
	char	str2[139] = "000000000000000000000000000000000";
	char	*str1 = "Tu trouves cette chaine longue toi? Plutot oui. Je pense qu'elle pourrait etre un peut plus longue quand meme. Carrement? Oui tout a fait.";
	char	str3[0] = "";
	int		ret;

	printf("This is a ft_strcmp test with VALID variables:\n");
	ret = strcmp(str2, str1);
	printf("Expected:\tstr1: %s\tstr2: %s\tReturn: %d\n", str1, str2, ret);
	ret = ft_strcmp(str2, str1);
	printf("Real:\t\tstr1: %s\tstr2: %s\tReturn: %d\n", str1, str2, ret);
	printf("This is a ft_strcmp test with EMPTY variables:\n");
	ret = strcmp(str3, str1);
	printf("Expected:\tstr3: %s\tstr2: %s\tReturn: %d\n", str3, str2, ret);
	ret = ft_strcmp(str3, str1);
	printf("Real:\t\tstr3: %s\tstr2: %s\tReturn: %d\n\n\n", str3, str2, ret);
}

void	ft_write_test()
{
	int		fd;
	char	*str1 = "Hello!\t";
	char	*str3 = 0;
	int		ret;

	printf("This is a ft_write test with VALID variables:\n");
	fd = open("test.txt", O_CREAT | O_WRONLY, 0777);
	ret = write(fd, str1, 7);
	printf("Expected:\tReturn: %d\tString: %s\tErrno: %d\n\n", ret, str1, errno);
	close(fd);
	fd = open("test.txt", O_CREAT | O_WRONLY, 0777);
	ret = ft_write(fd, str1, 7);
	printf("Real:\t\tReturn: %d\tString: %s\tErrno: %d\n\n", ret, str1, errno);
	close(fd);
	printf("This is a ft_write test with INVALID variables:\n");
	fd = open("test.txt", O_CREAT | O_WRONLY, 0777);
	ret = write(fd, str3, 7);
	printf("Expected:\tReturn: %d\tString: %s\tErrno: %d\n\n\n", ret, str3, errno);
	close(fd);
	fd = open("test.txt", O_CREAT | O_WRONLY, 0777);
	ret = ft_write(fd, str3, 7);
	printf("Real:\t\tReturn: %d\tString: %s\tErrno: %d\n\n\n", ret, str3, errno);
	close(fd);
}

void	ft_read_test()
{
	int		fd;
	char	*str1;
	char	*str2;
	int		ret;

	str1 = malloc(6);
	str2 = malloc(6);
	printf("This is a ft_read test with VALID variables:\n");
	fd = open("test.txt", O_CREAT | O_RDONLY, 0777);
	ret = read(fd, str1, 6);
	printf("Expected:\tReturn: %d\tString: %s\tErrno: %d\n\n", ret, str1, errno);
	close(fd);
	fd = open("test.txt", O_CREAT | O_RDONLY, 0777);
	ret = ft_read(fd, str1, 6);
	printf("Real:\t\tReturn: %d\tString: %s\tErrno: %d\n\n", ret, str1, errno);
	close(fd);
	printf("This is a ft_read test with INVALID variables:\n");
	fd = open("test.txt", O_CREAT | O_RDONLY, 0777);
	ret = read(fd, str2, -1);
	printf("Expected:\tReturn: %d\tString: %s\tErrno: %d\n\n\n", ret, str2, errno);
	close(fd);
	fd = open("test.txt", O_CREAT | O_RDONLY, 0777);
	ret = ft_read(fd, str2, -1);
	printf("Real:\t\tReturn: %d\tString: %s\tErrno: %d\n\n\n", ret, str2, errno);
	close(fd);
}

void	ft_strdup_test()
{
	char	str1[139] = "Tu trouves cette chaine longue toi? Plutot oui. Je pense qu'elle pourrait etre un peut plus longue quand meme. Carrement? Oui tout a fait.";
	char	str3[2] = "";
	char	*str2;

	printf("This is a ft_strdup test with VALID variables:\n");
	str2 = strdup(str1);
	printf("Expected:\tString: %s\n\n", str2);
	str2 = ft_strdup(str1);
	printf("Real:\t\tString: %s\n\n", str2);
	printf("This is a ft_strdup test with EMPTY variables:\n");
	str2 = strdup(str3);
	printf("Expected:\tString: %s\n\n", str2);
	str2 = ft_strdup(str3);
	printf("Real:\t\tString: %s\n\n\n", str2);
}

void	ft_atoi_base_test()
{
	char	*str1 = "   ++-dvfjk";
	char	*base = "dvfr";
	char	*base1 = "dvffr";
	int		a;

	printf("This is a ft_atoi_base test with VALID variables:\n");
	a = ft_atoi_base(str1, base);
	printf("Return: %d\n\n", a);
	printf("This is a ft_atoi_base test with INVALID variables:\n");
	a = ft_atoi_base(str1, base1);
	printf("Return: %d\n\n\n", a);
}

void	print_list(t_list *list)
{
	t_list	*temp;

	temp = list;
	printf("List:  ");
	while (temp != 0)
	{
		printf("%s  |  ", temp->data);
		temp = temp->next;
	}
	printf("\n\n");
}

void	ft_list_push_front_test()
{
	t_list	*list;
	char 	*chr = "S!mple";
	char	*chr1 = "";

	list = malloc(sizeof(t_list));
	list->data = chr;
	list->next = 0;
	printf("This is a ft_list_push_front test with VALID variables:\n");
	print_list(list);
	ft_list_push_front(&list, chr);
	print_list(list);
	printf("This is a ft_list_push_front test with EMPTY variables:\n");
	ft_list_push_front(&list, chr1);
	print_list(list);
	printf("\n");
	free(list->next->next);
	free(list->next);
	free(list);
}

void	ft_list_size_test()
{
	t_list	*list;
	char 	*chr = "S!mple";

	list = malloc(sizeof(t_list));
	list->data = chr;
	list->next = 0;
	printf("This is a ft_list_size test with VALID variable:\n");
	ft_list_push_front(&list, chr);
	printf("List Size: %d\n\n", ft_list_size(list));
	printf("This is a ft_list_size test with EMPTY variable:\n");
	free(list->next);
	free(list);
	list = 0;
	printf("List Size: %d\n\n\n", ft_list_size(list));
}

void	ft_list_sort_test()
{
	t_list	*list;

	list = malloc(sizeof(t_list));
	list->data = "B";
	list->next = malloc(sizeof(t_list));
	list->next->data = "Z";
	list->next->next = malloc(sizeof(t_list));
	list->next->next->data = "F";
	list->next->next->next = malloc(sizeof(t_list));
	list->next->next->next->data = "A";
	list->next->next->next->next = 0;

	printf("This is a ft_list_sort test with VALID variable:\n");
	print_list(list);
	ft_list_sort(&list, ft_strcmp);
	print_list(list);
	printf("\n");
	free(list->next->next->next);
	free(list->next->next);
	free(list->next);
	free(list);
}

void	ft_list_remove_if_test()
{
	t_list	*list;
	char *chr = malloc(2);
	char *chr1 = malloc(2);
	char *chr2 = malloc(2);
	char *chr3 = malloc(2);
	char *chr4 = malloc(2);
	chr[0] = 'F';
	chr1[0] = 'B';
	chr2[0] = 'A';
	chr3[0] = 'F';
	chr4[0] = 'F';
	chr[1] = '\0';
	chr1[1] = '\0';
	chr2[1] = '\0';
	chr3[1] = '\0';
	chr4[1] = '\0';

	list = malloc(sizeof(t_list));
	list->data = chr;
	list->next = malloc(sizeof(t_list));
	list->next->data = chr1;
	list->next->next = malloc(sizeof(t_list));
	list->next->next->data = chr2;
	list->next->next->next = malloc(sizeof(t_list));
	list->next->next->next->data = chr3;
	list->next->next->next->next = 0;
	
	printf("This is a ft_list_remove_if test with VALID variable (removing data with F):\n");
	print_list(list);
	ft_list_remove_if(&list, chr4, ft_strcmp, free);
	print_list(list);
	printf("\n");
	free(list->next->data);
	free(list->next);
	free(list->data);
	free(list);
}

int	main()
{
	printf("Testing...:\n\n");
	ft_strlen_test();
	ft_strcpy_test();
	ft_strcmp_test();
	ft_write_test();
	ft_read_test();
	ft_strdup_test();
	ft_atoi_base_test();
	ft_list_push_front_test();
	ft_list_size_test();
	ft_list_sort_test();
	ft_list_remove_if_test();
	return (0);
}
