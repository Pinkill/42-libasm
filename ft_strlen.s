; **************************************************************************** #
;                                                                              #
;                                                         :::      ::::::::    #
;    ft_strlen.s                                        :+:      :+:    :+:    #
;                                                     +:+ +:+         +:+      #
;    By: mknezevi <mknezevi@student.42.fr>          +#+  +:+       +#+         #
;                                                 +#+#+#+#+#+   +#+            #
;    Created: 2020/09/23 16:59:51 by mknezevi          #+#    #+#              #
;    Updated: 2020/09/26 13:30:26 by mknezevi         ###   ########.fr        #
;                                                                              #
; **************************************************************************** #

global _ft_strlen
						;build with nasm -f elf64 source.a -o output.o
						;link to executable with ld -m elf_x86_64 output.o -o executable
						;without linking, building for gcc gcc -m64 obj.o main.c -o ex
						;mov eax 1 eax is used for the type of system call, 1 being exit call
						;mov ebx 42 ebx is used as a exit status for our program, in this case 42
						;int 0x80 it is calling an interupt handler, in this case 0x80, which is a system call handler

section .text			;Start of code section

_ft_strlen:				;Function name ft_strlen
	push rbp			;Function prologue, saving the stack status before function call
	mov rbp, rsp		;~
	mov rax, 0			;Function return register set to 0
	jmp .loop			;Jumping to loop

	.loop_content:			;If compare is false, return register is increased
		inc rax

	.loop:							;The loop
		mov dl, byte [rdi+rax]		;We take each byte [letter] from first argument register rdi
		cmp dl, 0x0					;We check if the byte [letter] is 0 [end of string]
		jne .loop_content			;If dl isn't 0, it jumps to loop_content

	.end:							;Function epilogue, pulling the stack status before function call
		mov rsp, rbp
		pop rbp
		ret
