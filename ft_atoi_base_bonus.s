; **************************************************************************** #
;                                                                              #
;                                                         :::      ::::::::    #
;    ft_atoi_base_bonus.s                               :+:      :+:    :+:    #
;                                                     +:+ +:+         +:+      #
;    By: mknezevi <mknezevi@student.42.fr>          +#+  +:+       +#+         #
;                                                 +#+#+#+#+#+   +#+            #
;    Created: 2020/10/19 19:47:51 by mknezevi          #+#    #+#              #
;    Updated: 2020/11/01 04:41:16 by mknezevi         ###   ########.fr        #
;                                                                              #
; **************************************************************************** #

global _ft_atoi_base

;int	ft_atoi_base(char *str, char *base);

section .text
	extern _ft_isspace
	extern _ft_strlen

_ft_atoi_base:
	push rbp			;Function prologue, saving the stack status before function call
	mov rbp, rsp		;~
	mov rax, 0x0		;Setting return value to 0
	mov r9, 0x0			;R9 - base length, R10 - actual string length (to calculate), R12 - Temporary variable (pushed since it is reserved)
	mov r10, 0x0
	push r12
	cmp rsi, 0x0		;Check if arguments are set, if not jump to end
	je .end
	cmp rdi, 0x0
	je .end
	jmp .check_base

	.check_base_inner:			;Function checks if the character in base[rsi] argument is duplicated
		mov r8b, byte [rsi+rcx]
		cmp r8b, dl
		je .invalid
		cmp r8b, 0x0
		je .check_base
		inc rcx
		jmp .check_base_inner

	.check_base:
		mov dl, byte [rsi+r9]	;Getting one byte from rsi, on rax index
		cmp dl, 0x0				;Check if the character is the end of line
		je .length_set
		cmp dl, 43				;Check if the character is a plus sign
		je .invalid
		cmp dl, 45				;Check if the character is a minus sign
		je .invalid
		push rdi				;Pushing rdi to the stack, they are used by ft_isspace
		movsx rdi, dl			;Set the function argument to the character
		call _ft_isspace		;Calling function ft_isspace, returns true or false
		pop rdi					;Pulling the value back from stack
		cmp rax, 1				;Check if return is true
		je .invalid
		inc r9					;Incrementing r9 and moving it to rcx
		mov rcx, r9
		jmp .check_base_inner	;Check for duplicates of this valid character

	.length_set:
		cmp r9, 1				;Check that length of base is above 1
		jle .invalid
		call _ft_strlen			;Calling ft_strlen, rdi is already set, dl and rax values don't matter
		cmp rax, 0				;Check that rdi string isn't empty
		je .invalid
		mov r11, 1				;A multiplier to set number positive or negative
		jmp .scroll_spaces

	.scroll_spaces:				;Scroll through spaces, pluses and keep count of minuses until a different character appears or the end of line
		mov dl, byte [rdi]
		cmp dl, 0x0
		je .end
		cmp dl, 43
		je .skip_character
		cmp dl, 45
		je .is_minus
		push rdi
		movsx rdi, dl
		call _ft_isspace
		pop rdi
		cmp rax, 1
		je .skip_character
		mov rax, 0				;Set return value to 0
		jmp .scroll_string

	.is_minus:
		imul r11, -1
		jmp .skip_character

	.skip_character:
		inc rdi
		jmp .scroll_spaces	

	.scroll_string:
		mov dl, byte [rdi+r10]
		cmp dl, 0x0
		je .calculate
		mov rcx, 0				;Index of base[rsi]
		call is_in_base
		cmp rcx, -1
		je .calculate
		inc r10
		jmp .scroll_string

	.calculate:
		mov dl, byte [rdi]
		inc rdi				;Move the string up by a byte
		dec r10				;Reduce string length needed to transform
		cmp r10, 0x0		;Special check for last power to 0, which is 1
		mov rcx, 0
		jle .mul_one
		call is_in_base
		mov r8, 1			;Temporary variable to calculate from custom base to decimal base
		mov r12, r10
		jmp .pow

	.pow:
		imul r8, r9
		dec r12
		cmp r12, 0x0
		jg .pow
		imul r8, rcx
		add rax, r8
		jmp .calculate

	.mul_one:
		call is_in_base
		mov r8, 1			;Temporary variable to calculate from custom base to decimal base
		imul r8, rcx
		add rax, r8
		jmp .mul_minus

	.mul_minus:
		imul rax, r11
		jmp .end

	.invalid:		;Function for invalid arguments
		mov rax, 0
		jmp .end

	.end:					;Function epilogue, pulling the stack status before function call
		pop r12
		mov rsp, rbp
		pop rbp
		ret

is_in_base:
	mov r8b, byte[rsi+rcx]
	cmp r8b, 0x0
	je .found_no_base
	cmp dl, r8b
	je .found_base
	inc rcx
	jmp is_in_base

	.found_no_base:
		mov rcx, -1
		jmp .found_base

	.found_base:
		ret
