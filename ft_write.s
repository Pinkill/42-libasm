; **************************************************************************** #
;                                                                              #
;                                                         :::      ::::::::    #
;    ft_write.s                                         :+:      :+:    :+:    #
;                                                     +:+ +:+         +:+      #
;    By: mknezevi <mknezevi@student.42.fr>          +#+  +:+       +#+         #
;                                                 +#+#+#+#+#+   +#+            #
;    Created: 2020/09/30 14:55:20 by mknezevi          #+#    #+#              #
;    Updated: 2020/10/27 17:41:02 by mknezevi         ###   ########.fr        #
;                                                                              #
; **************************************************************************** #

%include "os_specific_code.mac"

global _ft_write

section .text

extern ERR_NO

;ssize_t	ft_write(int fd, const void *buf, size_t count)
;id: 1(linux), sys_write((rdi)fd, (rsi)buffer, (rdx)count)

_ft_write:
	push rbp			;Function prologue, saving the stack status before function call
	mov rbp, rsp		;~
	mov rax, SYS_WRITE	;Performing system call, 1 (on linux) -> sys_write
						;The three arguments should be already in registers at call, rdi, rsi, rdx
	syscall				;Performing system call
	%ifdef IS_APPLE
		jc .error
	%else
		test rax, rax		;Performing bitwise and operation on rax
		js .error			;Jump to error if it is true, checking last bits, ie. checking if number is negative
	%endif
	jmp .end

	.error:
		push rax				;Saving the error number value output
		call ERR_NO				;Returns a pointer to errno variable
		pop qword [rax]			;Setting the value of pointer to error number
		%ifndef IS_APPLE
			neg qword [rax]			;Since error number outputs from system calls are always negative (linux), it must be set positive for proper use
		%endif
		mov rax, -1				;Setting function output -1, an error occured
		jmp .end

	.end:					;Function epilogue, pulling the stack status before function call
		mov rsp, rbp
		pop rbp
		ret