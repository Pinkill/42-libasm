; **************************************************************************** #
;                                                                              #
;                                                         :::      ::::::::    #
;    ft_list_sort.s                                     :+:      :+:    :+:    #
;                                                     +:+ +:+         +:+      #
;    By: mknezevi <mknezevi@student.42.fr>          +#+  +:+       +#+         #
;                                                 +#+#+#+#+#+   +#+            #
;    Created: 2020/10/19 19:48:10 by mknezevi          #+#    #+#              #
;    Updated: 2020/10/19 20:06:22 by mknezevi         ###   ########.fr        #
;                                                                              #
; **************************************************************************** #

global _ft_list_sort

;void	ft_list_sort(t_list **begin_list, int(*cmp)());
;cmp being (*cmp)(list_ptr->data, list_other_ptr->data);
;This function uses merge sort

section .text

_ft_list_sort:
	push rbp				;Function prologue, saving the stack status before function call
	mov rbp, rsp			;~
	test rdi, rdi			;Check if pointer to pointer is null
	jz .end
	test rsi, rsi			;Check if function pointer is null
	jz .end
	mov r10, rdi			;R10 - linked list
	mov r11, rsi			;R11 - function pointer
	mov rdx, [r10]			;Get pointer out of pointer to pointer
	call merge_sort
	mov [r10], rdx
	jmp .end

	.end:					;Function epilogue, pulling the stack status before function call
		mov rsp, rbp
		pop rbp
		ret

merge_sort:
	test rdx, rdx
	jz .return
	cmp qword [rdx+8], 0x0
	je .return
	call front_back_split
	push r9
	push rdx
	mov rdx, r8
	call merge_sort
	mov r8, rdx
	pop rdx
	pop r9
	push rdx
	push r8
	mov rdx, r9
	call merge_sort
	mov r9, rdx
	pop r8
	pop rdx
	call sorted_merge

	.return:
		ret

front_back_split:
	mov rdi, rdx				;RDI (slow) has to contain the last element to midpoint in list
	mov rsi, [rdx+8]			;RSI (fast) has to contain the last element after midpoint in list
	jmp .split_loop

	.split_loop:
		test rsi, rsi
		jz .front_back_split_end
		mov rsi, [rsi+8]
		test rsi, rsi
		jz .front_back_split_end
		mov rdi, [rdi+8]
		mov rsi, [rsi+8]
		jmp .split_loop

	.front_back_split_end:
		mov r8, rdx
		mov r9, [rdi+8]
		mov qword [rdi+8], 0x0
		ret

sorted_merge:
	test r8, r8
	jz .return_r9
	test r9, r9
	jz .return_r8
	mov rdi, [r8]
	mov rsi, [r9]
	push rdx
	call r11
	pop rdx
	cmp rax, 0
	jle .is_lower
	mov rdx, r9
	push rdx
	mov r9, [r9+8]
	call sorted_merge
	mov rdi, rdx
	pop rdx
	mov [rdx+8], rdi
	ret

	.is_lower:
		mov rdx, r8
		push rdx
		mov r8, [r8+8]
		call sorted_merge
		mov rdi, rdx
		pop rdx
		mov [rdx+8], rdi
		ret

	.return_r8:
		mov rdx, r8
		ret

	.return_r9:
		mov rdx, r9
		ret
