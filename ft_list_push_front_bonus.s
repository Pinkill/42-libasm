; **************************************************************************** #
;                                                                              #
;                                                         :::      ::::::::    #
;    ft_list_push_front_bonus.s                         :+:      :+:    :+:    #
;                                                     +:+ +:+         +:+      #
;    By: mknezevi <mknezevi@student.42.fr>          +#+  +:+       +#+         #
;                                                 +#+#+#+#+#+   +#+            #
;    Created: 2020/10/19 19:48:01 by mknezevi          #+#    #+#              #
;    Updated: 2020/11/01 04:39:08 by mknezevi         ###   ########.fr        #
;                                                                              #
; **************************************************************************** #

%include "os_specific_code.mac"

global _ft_list_push_front

;void	ft_list_push_front(t_list **begin_list, void *data);

section .text
	extern MALLOC

_ft_list_push_front:
	push rbp				;Function prologue, saving the stack status before function call
	mov rbp, rsp			;~
	test rdi, rdi			;Check if pointer to pointer is null
	jz .end
	push rdi				;Pushing rdi and rsi (for some reason malloc zeroed rsi) on stack to set malloc argument
	push rsi
	mov rdi, 16				;Make a pointer to 16 bit reserved space
	call MALLOC
	pop rsi					;Pull rdi, rsi back from stack
	pop rdi
	test rax, rax			;Check if malloc failed
	jz .end
	mov qword [rax], rsi	;Setting structure field to argument value
	mov rdx, [rdi]			;rdi is pointer to pointer for protection, so we have to get the pointer
	test rdx, rdx			;Check if pointer is 0
	jz .create_list
	mov qword [rax+8], rdx	;Setting the field "next" to point to beggining of the chained t_list
	mov [rdi], rax			;Replace the argument with starting chained list pointer
	jmp .end

	.create_list:
		mov qword [rax+8], 0x0	;Set "next" as null
		mov [rdi], rax			;Set the created pointer as starting pointer
		jmp .end

	.end:					;Function epilogue, pulling the stack status before function call
		mov rsp, rbp
		pop rbp
		ret
