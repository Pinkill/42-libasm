# 42 libasm

A project in the 42 study program. This project is an introduction into assembly language. The final result is a static library consisting of string handling function, read/write functions and linked list handling functions.

To see the documentation of the program and how it works, check "en.subject.pdf" file.

## Getting Started

Most of the functions in this project are re-codings of c library functions, but there are a few custom ones as well.

I also added a simple test file, to test the functions. (Read Testing the library section).

#### Custom functions

Ft_atoi_base:

```
int	ft_atoi_base(char *str, char *base);
```

- This function takes a string and calculates it into a number based on characters in base. The string can have as many spaces at the beginning and as many minuses/pluses after the spaces. If the amount of minuses is an odd number, the result will be negative.

- After the spaces and plus/minuses the rest of the characters are read until it finds a character that is not in base.

- The base cannot have duplicates of characters or plus/minus/space.

Example:
```
string: "   ++-+dvfjk" base: "dvfr" result: "-6"
```

Linked list functions:

```
void	ft_list_push_front(t_list **begin_list, void *data);
int     ft_list_size(t_list *begin_list);
void	ft_list_remove_if(t_list **begin_list, void *data_ref, int (*cmp)(), void (*free_fct)(void*));
```

- These are pretty straight forward.

Linked list sort function:

```
void	ft_list_sort(t_list **begin_list, int(*cmp)());
```

- It sorts the linked list by their data. I tested it only with ft_strcmp as function pointer. So, I don't know how well it will work with the rest. Might need some adjustments.

- It uses merge sort, so it sorts in O(n log n) time complexity.

### Prerequisites

For linux and mac:

```
- nasm
- clang (for testing)
- ar
- ranlib
```

### Installing

Getting the project:

```
git clone https://gitlab.com/Pinkill/42-libasm libasm
```

#### Building the static library

```
make all
```

#### Cleaning, rebuilding the project:

Removes library, but not the objects

```
make clean
```

Cleans whole project

```
make fclean
```

Cleans and rebuilds whole project

```
make re
```

### Testing the library

If you have setup clang and want to see some simple test for these functions. You can run:

```
make run
```

Mind you, these are just some simple tests, nothing too in depth.

Good luck!