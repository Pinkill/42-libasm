; **************************************************************************** #
;                                                                              #
;                                                         :::      ::::::::    #
;    ft_strdup.s                                        :+:      :+:    :+:    #
;                                                     +:+ +:+         +:+      #
;    By: mknezevi <mknezevi@student.42.fr>          +#+  +:+       +#+         #
;                                                 +#+#+#+#+#+   +#+            #
;    Created: 2020/10/19 19:48:19 by mknezevi          #+#    #+#              #
;    Updated: 2020/11/01 04:37:12 by mknezevi         ###   ########.fr        #
;                                                                              #
; **************************************************************************** #

%include "os_specific_code.mac"

global _ft_strdup

;char	*strdup(const char *s);

section .text
	extern MALLOC
	extern _ft_strlen
	extern _ft_strcpy

_ft_strdup:
	push rbp					;Function prologue, saving the stack status before function call
	mov rbp, rsp				;~
	mov qword [str_ref], rdi	;We save the rdi to a variable
	call _ft_strlen				;Get length of rdi string
	inc rax						;Incrementing rax, for that last null byte
	mov rdi, rax				;Move it to rdi, as first parameter of malloc
	imul rdi, 8					;Multiply it with 8, each byte has 8 bits
	call MALLOC					;Create a pointer, with needed length
	test rax, rax				;Check if malloc failed
	jz .end
	mov rsi, qword [str_ref]	;Put the str_ref on rsi as second parameter
	mov rdi, rax				;Pass pointer to rdi as first argument to ft_strcpy
	call _ft_strcpy				;Copy string from rsi to rdi, rax points to same address as rdi
	jmp .end

	.end:						;Function epilogue, pulling the stack status before function call
		mov rsp, rbp
		pop rbp
		ret

section .data
	str_ref	dq	0
